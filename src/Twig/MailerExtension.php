<?php

namespace Kalitics\NotificationBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class MailerExtension extends AbstractExtension{

    public function getFilters() {
        return [
            new TwigFilter(
                'lastMail',
                [
                    NotificationRuntime::class,
                    'lastMail',
                ],
                ['is_safe' => ['html']]
            ),
        ];
    }

    public function getFunctions() {
        return [
            new TwigFunction(
                'mailerRouteShow',
                [
                    NotificationRuntime::class,
                    'mailerRouteShow',
                ]
            ),
        ];
    }
}
