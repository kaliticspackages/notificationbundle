<?php


namespace Kalitics\NotificationBundle\Twig;

use Doctrine\ORM\EntityManagerInterface;
use Kalitics\NotificationBundle\Entity\Mail;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\RuntimeExtensionInterface;

use function get_class;
use function is_object;

class NotificationRuntime implements RuntimeExtensionInterface
{
    /** @var \Symfony\Component\Routing\RouterInterface  */
    private RouterInterface $router;

    /** @var \Doctrine\ORM\EntityManagerInterface  */
    private EntityManagerInterface $entityManager;

    public function __construct(
        RouterInterface $router,
        EntityManagerInterface $entityManager
    ) {
        $this->router = $router;
        $this->entityManager = $entityManager;
    }

    public function lastMail($object)
    {
        $mail = null;
        if (is_object($object)) {
            try {
                $class = get_class($object);
                $mail = $this->entityManager->getRepository(Mail::class)->findOneBy(['entityClass' => $class, 'entityId' => $object->getId()], ['createdAt' => 'DESC']);
            } catch (\Exception $e) {}
        }

        return $mail;
    }

    public function mailerRouteShow(Mail $mail)
    {
        if ($mail->getEntityClass() && $mail->getEntityId()) {
            $emailableRoute = $mail->getEntityClass()::getEmailableRouteShow();
            if ($emailableRoute) {
                $params = [];
                foreach ($emailableRoute['params'] as $param) {
                    if ($param === 'id') {
                        $params[$param] = $mail->getEntityId();
                    }
                }
                return $this->router->generate($emailableRoute['route'], $params);
            }
        }

        return '';
    }
}
