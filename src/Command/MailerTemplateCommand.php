<?php

namespace Kalitics\NotificationBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Kalitics\NotificationBundle\Entity\MailerTemplate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Twig\Environment;

class MailerTemplateCommand extends Command
{
    protected static $defaultName = 'kalitics:notification:mailertemplate';

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var Environment
     */
    private Environment $twig;

    public function __construct(EntityManagerInterface $entityManager, Environment $twig)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->twig = $twig;
    }

    public function configure()
    {
        $this
            ->setDescription('Mailer template')
            ->addOption(
                'force',
                null,
                InputOption::VALUE_NONE
            )
            ->addOption(
                'slug',
                null,
                InputOption::VALUE_OPTIONAL
            )
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $process = false;
        $force = $input->getOption('force');
        $slug = $input->getOption('slug') ? $input->getOption('slug') : 'DEFAULT';

        $default = $this->entityManager->getRepository(MailerTemplate::class)->findOneBy(['slug' => $slug]);
        if (!$default) {
            $process = true;
            $default = new MailerTemplate();
        } elseif ($default && $force) {
            $process = true;
        } elseif ($default && !$force) {
            $output->writeln('Default template already exists use --force');
        }

        if ($process) {
            $template = $this->twig->loadTemplate('@KaliticsNotification/templating/mailertemplate.html.twig');
            $header = $template->renderBlock('header', []);
            $footer = $template->renderBlock('footer', []);

            $default->setHeader($header)
                ->setFooter($footer)
                ->setSlug($slug)
                ->setTitle('_'.$slug)
                ->setDescription('default template')
                ->setDefaultTo([])
                ->setDefaultFrom([])
                ->setDefaultReplyto([])
                ->setDefaultCc([])
            ;

            $this->entityManager->persist($default);
            $this->entityManager->flush();
        }
    }
}
