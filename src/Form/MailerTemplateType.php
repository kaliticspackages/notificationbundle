<?php

namespace Kalitics\NotificationBundle\Form;

use Kalitics\NotificationBundle\Entity\MailableEntity;
use Kalitics\NotificationBundle\Entity\MailerTemplate;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailerTemplateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = $options['choices'];

        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre',
            ])
            ->add('description', TextType::class, [
                'label' => 'Description',
            ])
            ->add('header', TextareaType::class, [
                'label' => 'Header',
            ])
            ->add('footer', TextareaType::class, [
                'label' => 'Footer',
            ])
            ->add('subject', TextType::class, [
                'label' => 'Sujet',
            ])
            ->add('mailableEntity', EntityType::class, [
                'label' => 'Lier à',
                'class' => MailableEntity::class,
                'required' => false,
                'placeholder' => '-- Sélectionner un lien --'
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Contenu',
            ])
            ->add('defaultAttachments', CollectionType::class, [
                'label' => 'Pièce jointe par défaut',
                'entry_type' => MailerTemplateAttachmentType::class,
                'entry_options' => [
                    'label' => false,
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'allow_file_upload' => true,
                'prototype' => true,
                'by_reference' => false,
            ])
            ->add('readonlyTo', CheckboxType::class, [
                'required' => false,
                'label' => 'Lecture seule ?'
            ])
            ->add('readonlyFrom', CheckboxType::class, [
                'required' => false,
                'label' => 'Lecture seule ?'
            ])
            ->add('readonlyReplyto', CheckboxType::class, [
                'required' => false,
                'label' => 'Lecture seule ?'
            ])
            ->add('readonlyCc', CheckboxType::class, [
                'required' => false,
                'label' => 'Lecture seule ?'
            ])
            ->add('showTo', CheckboxType::class, [
                'required' => false,
                'label' => 'Afficher ?'
            ])
            ->add('showFrom', CheckboxType::class, [
                'required' => false,
                'label' => 'Afficher ?'
            ])
            ->add('showReplyto', CheckboxType::class, [
                'required' => false,
                'label' => 'Afficher ?'
            ])
            ->add('showCc', CheckboxType::class, [
                'required' => false,
                'label' => 'Afficher ?'
            ])
        ;

        $modifierFunction = (function ($fields, $choices) use ($builder) {
            foreach ($fields as $key => $value) {
                $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($key, $value, $choices) {

                    $form = $event->getForm();
                    if (isset($event->getData()[$key])) {
                        foreach ($event->getData()[$key] as $email) {
                            if (!in_array($email, $choices)) {
                                $choices[$email] = $email;
                            }
                        }
                    }

                    $form->add($key, ChoiceType::class, [
                        'label' => $value,
                        'required' => false,
                        'multiple' => true,
                        'expanded' => false,
                        'choices' => $choices,
                    ]);

                });

                $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($key, $value, $choices) {

                    $form = $event->getForm();
                    if ($event->getData()->{'get'.$key}()) {
                        foreach ($event->getData()->{'get'.$key}() as $email) {
                            if (!in_array($email, $choices)) {
                                $choices[$email] = $email;
                            }
                        }
                    }

                    $form->add($key, ChoiceType::class, [
                        'label' => $value,
                        'required' => false,
                        'multiple' => true,
                        'expanded' => false,
                        'choices' => $choices,
                    ]);

                });
            }
        });

        $modifierFunction([
            'defaultTo' => 'A',
            'defaultFrom' => 'De',
            'defaultReplyto' => 'Répondre à',
            'defaultCc' => 'Cc',
        ], $choices);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MailerTemplate::class,
            'choices' => [],
        ]);
    }
}
