<?php

namespace Kalitics\NotificationBundle\Form;

use Kalitics\NotificationBundle\Entity\Mail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailPreviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = $options['choices'];

        $builder
            ->add('subject', TextType::class)
            ->add('content', TextareaType::class)
        ;

        $modifierFunction = (function ($fields, $choices) use ($builder) {
            foreach ($fields as $key => $value) {
                $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($key, $value, $choices) {

                    $form = $event->getForm();
                    if (isset($event->getData()[$key])) {
                        foreach ($event->getData()[$key] as $email) {
                            if (!in_array($email, $choices)) {
                                $choices[$email] = $email;
                            }
                        }
                    }

                    $form->add($key, ChoiceType::class, [
                        'label' => $value,
                        'required' => false,
                        'multiple' => true,
                        'expanded' => false,
                        'choices' => $choices,
                    ]);

                });

                $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($key, $value, $choices) {

                    $form = $event->getForm();
                    if ($event->getData()->{'get'.$key}()) {
                        foreach ($event->getData()->{'get'.$key}() as $email) {
                            if (!in_array($email, $choices)) {
                                $choices[$email] = $email;
                            }
                        }
                    }

                    $form->add($key, ChoiceType::class, [
                        'label' => $value,
                        'required' => false,
                        'multiple' => true,
                        'expanded' => false,
                        'choices' => $choices,
                    ]);

                });
            }
        });

        $modifierFunction([
            'to' => 'A',
            'from' => 'De',
            'replyto' => 'Répondre à',
        ], $choices);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mail::class,
            'choices' => [],
        ]);
    }
}
