<?php

namespace Kalitics\NotificationBundle\Form;

use Kalitics\NotificationBundle\Entity\Mail;
use Kalitics\NotificationBundle\Entity\MailerTemplate;
use Kalitics\NotificationBundle\Repository\MailerTemplateRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = $options['choices'];
        $mailableClass = $options['mailableClass'];
        $linked = $options['linked'];
        $object = $options['object'];

        if ($mailableClass) {
            $builder->add('mailerTemplate', EntityType::class, [
                'class' => MailerTemplate::class,
                'placeholder' => '-- Sélectionner un modèle --',
                'required' => false,
                'query_builder' => function (MailerTemplateRepository $mailerTemplateRepository) use ($mailableClass) {
                    $qb = $mailerTemplateRepository->createQueryBuilder('mailer_template')
                        ->join('mailer_template.mailableEntity', 'mailable_entity')
                        ->where('mailable_entity.slug = :mailableClass')
                        ->setParameter('mailableClass', $mailableClass)
                    ;
                    return $qb;
                }
            ]);
        }

        if ($linked) {
            $builder->add('linked', $linked, [
                'mapped' => false,
                'data' => $object,
            ]);
        } else {
            $builder
                ->add('subject', TextType::class)
                ->add('content')
            ;
        }

        $modifierFunction = (function ($fields, $choices) use ($builder) {
            foreach ($fields as $key => $value) {
                $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($key, $value, $choices) {

                    $form = $event->getForm();
                    if (isset($event->getData()[$key])) {
                        foreach ($event->getData()[$key] as $email) {
                            if (!in_array($email, $choices)) {
                                $choices[$email] = $email;
                            }
                        }
                    }

                    $form->add($key, ChoiceType::class, [
                        'label' => $value,
                        'required' => in_array($key, ['replyto', 'cc']) ? false : true,
                        'multiple' => true,
                        'expanded' => false,
                        'choices' => $choices,
                    ]);

                });

                $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($key, $value, $choices) {

                    $form = $event->getForm();
                    if ($event->getData()->{'get'.$key}()) {
                        foreach ($event->getData()->{'get'.$key}() as $email) {
                            if (!in_array($email, $choices)) {
                                $choices[$email] = $email;
                            }
                        }
                    }

                    $form->add($key, ChoiceType::class, [
                        'label' => $value,
                        'required' => in_array($key, ['replyto', 'cc']) ? false : true,
                        'multiple' => true,
                        'expanded' => false,
                        'choices' => $choices,
                    ]);

                });
            }
        });

        $modifierFunction([
            'cc' => 'CC',
            'to' => 'A',
            'from' => 'De',
            'replyto' => 'Répondre à',
        ], $choices);

        $builder->add('mailAttachments', CollectionType::class, [
            'label' => false,
            'entry_type' => MailAttachmentType::class,
            'entry_options' => [
                'label' => false,
            ],
            'allow_add' => true,
            'allow_delete' => true,
            'allow_file_upload' => true,
            'prototype' => true,
            'by_reference' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'data_class' => Mail::class,
            'mailableClass' => null,
            'choices' => null,
            'allow_file_upload' => true,
            'object' => null,
            'linked' => null,
            'validation_groups' => 'default',
        ]);
    }
}
