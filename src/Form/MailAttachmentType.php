<?php

namespace Kalitics\NotificationBundle\Form;

use Kalitics\NotificationBundle\Entity\MailAttachment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailAttachmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('file', FileType::class, [
                'label' => 'Fichier',
                'required' => true,
                'mapped' => false,
            ])
        ;

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            if (null != $event->getData()) {
                $event->getForm()->remove('file');
                $event->getForm()->add('file', FileType::class, [
                    'label' => 'Fichier',
                    'required' => false,
                    'mapped' => false,
                    'attr' => [
                        'data-filename' => $event->getData()->getFilename(),
                    ],
                ]);
            }
        });

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MailAttachment::class,
        ]);
    }
}
