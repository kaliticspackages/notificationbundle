<?php

namespace Kalitics\NotificationBundle\Service;

use App\Entity\Security\Group;
use App\Entity\User\User;
use App\Repository\User\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Kalitics\GedBundle\Entity\ConfigGedCategory;
use Kalitics\GedBundle\Entity\Ged;
use Kalitics\NotificationBundle\Entity\NotificationType;
use UserBundle\Entity\UserNotification;


class NotificationSubscriptionsService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var array
     */
    private $notificables;

    public function __construct(EntityManager $entityManager)
    {
        $this->notifiables              = array();
        $this->em                       = $entityManager;
        $this->userRepo                 = $this->em->getRepository(User::class);
    }


    public function getMembers(NotificationType $type){

        $this->notifiables = [];
        foreach($type->getGroups() as $group){
            $this->notifiables = array_merge($this->notifiables, $this->userRepo->findByGroup($group));
        }
        //Add included
        $this->notifiables = array_merge($this->notifiables, $type->getIncludedUsers()->toArray());
        //Remove excluded
        $this->notifiables = array_diff($this->notifiables, $type->getExcludedUsers()->toArray());

        return $this->notifiables;
    }


    public function userHasSubscription(User $user, NotificationType $type){
        //If user is included
        if($type->includedUsersContains($user)){
            return 'user';
        }
        //If user is in group and not excluded
        if(!$type->excludedUsersContains($user) && $type->groupsContains($user->getGroupRole())){
            return 'group '.$user->getGroupRole();
        }
        return false;
    }

    public function suscribeUsers(array $users, NotificationType $type, $order = null){

        $count = 0;
        foreach ($users as $user){
            if($order != null){
                $this->setSubscription($user, $type, $order);
            }else{
                if($this->userHasSubscription($user, $type)){
                    $this->setSubscription($user, $type, false);
                }else{
                    $this->setSubscription($user, $type, true);
                }
            }
            $count++;
        }

        return $count;
    }


    public function setSubscription(User $user, NotificationType $type, bool $setTo){

        if($setTo === true){
            if($type->excludedUsersContains($user)){
                $type->removeExcludedUser($user);
            }
            if(!$type->groupsContains($user->getGroupRole())){
                $type->addIncludedUser($user);
            }
        }

        if($setTo === false) {
            if ($type->includedUsersContains($user)) {
                $type->removeIncludedUser($user);
            }
            if ($type->groupsContains($user->getGroupRole())) {
                $type->addExcludedUser($user);
            }
        }

        $this->em->persist($type);
        $this->em->flush();

        return $this->userHasSubscription($user, $type);
    }
}
