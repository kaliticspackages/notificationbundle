<?php

namespace Kalitics\NotificationBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Kalitics\NotificationBundle\Entity\Mail;
use Kalitics\NotificationBundle\Entity\MailerTemplate;
use Swift_Attachment;
use Twig\Environment;

/**
 * Class MailerServiceBuilder
 * @package App\Service\Utilities
 */
class MailerService
{
    /**
     * @var \Swift_Mailer
     */
    protected $swiftMailer;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var Environment
     */
    protected $templating;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var array
     */
    protected $to;

    /**
     * @var array
     */
    protected $from;

    /**
     * @var array
     */
    protected $cc;

    /**
     * @var array
     */
    protected $replyto;

    /**
     * @var
     */
    protected $body;

    /**
     * @var
     */
    protected $contentType;

    /**
     * @var
     */
    protected $charset;

    /**
     * @var null
     */
    protected $attachments = [];

    /**
     * @var Mail
     */
    private $queued;

    /**
     * @var
     */
    private $linked;

    /**
     * MailerServiceBuilder constructor.
     * @param \Swift_Mailer $swiftMailer
     * @param EntityManagerInterface $entityManager
     * @param Environment $templating
     */
    public function __construct(
        \Swift_Mailer $swiftMailer,
        EntityManagerInterface $entityManager,
        Environment $templating
    )
    {
        $this->swiftMailer = $swiftMailer;
        $this->entityManager = $entityManager;
        $this->templating = $templating;

        $this->message = new \Swift_Message();
    }

    public function setQueued($queued)
    {
        $this->queued = $queued;

        return $this;
    }

    public function getQueued()
    {
        return $this->queued;
    }

    public function setLinked($linked)
    {
        $this->linked = $linked;

        return $this;
    }

    public function getLinked()
    {
        return $this->linked;
    }

    public function setSubject(string $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    public function setTo($to)
    {
        $this->to = $to;

        return $this;

    }

    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    public function setCc($cc)
    {
        $this->cc = $cc;

        return $this;
    }

    public function setReplyTo($replyto)
    {
        $this->replyto = $replyto;

        return $this;
    }

    public function setBody($body, $contentType = 'text/html', $charset = null)
    {
        $this->body        = $body;
        $this->contentType = $contentType;
        $this->charset     = $charset;

        return $this;
    }

    public function addAttachmentFile(Swift_Attachment $attachment){
        $this->attachments[] = $attachment;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function send()
    {
        foreach($this->attachments as $attachment) {
            $this->message->attach($attachment);
        }

        $this->message->setSubject($this->subject)
            ->setFrom($this->from)
            ->setTo($this->to)
            ->setCc($this->cc)
            ->setReplyTo($this->replyto)
            ->setBody($this->body, $this->contentType, $this->charset);

        $this->swiftMailer->send($this->message);
    }

    public function sendQueued($more = null)
    {
        /** @var Mail $mail */
        $mail = $this->getQueued();

        //$mail->setCreatedBy(null);
        $mail->setContent($mail->getContent() ."\n". $more);

        if ($mail->getMailerTemplate()) {
            $mail->setMessage($mail->getMailerTemplate()->getHeader().$mail->getContent().$mail->getMailerTemplate()->getFooter());
        } else {
            $mail->setMessage($mail->getContent());
        }

        $this->entityManager->persist($mail);
        $this->entityManager->flush();

        foreach ($mail->getAttachments() as $attachment) {
            $content = file_get_contents($attachment['attachment_path'].$attachment['filename']);
            $file = new \Swift_Attachment($content, $attachment['name']);
            $this->addAttachmentFile($file);
        }

        $this->setTo($mail->getTo())
            ->setFrom($mail->getFrom())
            ->setCc($mail->getCc())
            ->setReplyTo($mail->getReplyto())
            ->setSubject($mail->getSubject())
            ->setBody($mail->getMessage())
        ;

        $this->send();

    }

    public function updateLinked()
    {
        if ($this->getQueued() && $this->getQueued()->getMailerTemplate() && $this->getLinked()) {

            $mail = $this->getQueued();
            $template = $this->getQueued()->getMailerTemplate();
            $object = $this->getLinked();

            $subject = $template->getSubject();
            $content = $template->getContent();

            $mail->setSubject($this->valuationInjector($subject, $object));
            $mail->setContent($this->valuationInjector($content, $object, true));
        }

        return $this;
    }

    public function lastMail($object)
    {
        $mail = null;
        if (is_object($object)) {

            try {
                $class = get_class($object);
                $mail = $this->entityManager->getRepository(Mail::class)->findOneBy(['entityClass' => $class, 'entityId' => $object->getId()], ['createdAt' => 'DESC']);

            } catch (\Exception $e) {}
        }

        return $mail;
    }

    public function getMore($template, $params)
    {
        return $this->templating->render($template, $params);
    }

    public function sendLinked($object, $template_slug, $to, $template_more = null, $params = [])
    {
        if (!is_array($to)) {
            $to = [$to];
        }

        /** @var MailerTemplate $mailerTemplate */
        $mailerTemplate = $this->entityManager->getRepository(MailerTemplate::class)->findOneBy(['slug' => $template_slug]);

        $mail = new Mail();
        $mail->setMailerTemplate($mailerTemplate);
        $mail->setTo(array_merge($mailerTemplate->getDefaultTo(), $to));
        $mail->setFrom($mailerTemplate->getDefaultFrom());
        $mail->setCc($mailerTemplate->getDefaultCc());
        $mail->setReplyto($mailerTemplate->getDefaultReplyto());

        $this->setQueued($mail);
        $this->setLinked($object);

        $more = '';
        if ($template_more) {
            $more = $this->getMore($template_more, array_merge(['object' => $object], $params));
        }

        $this->updateLinked()->sendQueued($more);
    }

    public function valuationInjector($entry, $object = null, $nl2br = false)
    {
        if ($object) {
            $entry = preg_replace_callback('~{{(([^}][^}]?|[^}]}?)*)}}~', function ($match) use ($object, $nl2br) {
                $var = trim($match[1]);

                $founded = false;
                $class = get_class($object);
                $associations = $class::getEmailableAssociation();
                foreach ($associations as $association) {
                    if (isset($association[$var])) {
                        $founded = true;
                        break;
                    }
                }

                if (!$founded) {
                    return $var;
                }

                $nodes = explode('.', $var);

                $value = $object;
                try {
                    while ($key = array_shift($nodes)) {
                        $getter = 'get' . ucfirst($key);
                        $value = $value->{$getter}();
                    }
                } catch (\Throwable $e) {
                    return $var;
                }

                if ($value instanceof \DateTime) {
                    $value = $value->format('d/m/Y');
                } elseif (is_object($value)) {
                    try {
                        $value = $value->__toString();
                    } catch (\Exception $e) {
                        $value = 'ERROR';
                    }
                }

                if ($nl2br) {
                    $value = nl2br($value);
                }
                return $value;

            }, $entry);

        }
        return $entry;
    }
}
