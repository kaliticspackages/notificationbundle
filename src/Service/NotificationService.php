<?php

namespace Kalitics\NotificationBundle\Service;

use App\Entity\User\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Kalitics\GedBundle\Entity\ConfigGedCategory;
use Kalitics\GedBundle\Entity\Ged;
use Kalitics\NotificationBundle\Entity\Notification;
use Kalitics\NotificationBundle\Entity\NotificationSubscription;
use Kalitics\NotificationBundle\Entity\NotificationType;
use Kalitics\NotificationBundle\Entity\UserNotification;


class NotificationService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var array
     */
    private $notifications;

    /**
     * @var NotificationSubscriptionsService
     */
    private $subscriptionService;

    public function __construct(EntityManager $entityManager, NotificationSubscriptionsService $subscriptionsService)//EntityManagerInterface $entityManager
    {
        $this->notifications            = array();
        $this->em                       = $entityManager;
        $this->subscriptionService      = $subscriptionsService;
    }

    public function markAsSeen(UserNotification $userNotification){
        $userNotification->setSeenAt(new \DateTime("now"));
        $this->em->persist($userNotification);
        $this->em->flush();
        return $userNotification;
    }

    public function getNotifications(User $user)
    {
        $result = array();
        $this->notifications = $this->em->getRepository(UserNotification::class)->findBy(array(
            "user" => $user
        ));
        return $this->notifications;
    }

    public function generateNotification($notificationTypeSlug = null, $options = null){

        if($notificationTypeSlug != null){
            $notificationType = $this->em->getRepository(NotificationType::class)->findOneBy([
                "slug" => $notificationTypeSlug
            ]);
        }

        if($notificationType == null){
            return null;
        }

        //Format text
        $subject = $notificationType->getSubject();
        if (isset($options['subject']) && is_iterable($options['subject'])) {
            $subject = str_replace(array_keys($options['subject']), array_values($options['subject']), $subject);
        }

        $message = $notificationType->getMessage();
        if (isset($options['message']) && is_iterable($options['message'])) {
            $message = str_replace(array_keys($options['message']), array_values($options['message']), $message);
        }

        //Create notification
        $notification = new Notification();
        $notification->setType($notificationType);
        $notification->setSubject($subject);
        $notification->setMessage($message);

        if (isset($options['link'])) {
            $notification->setLink($options['link']);
        }

        $this->em->persist($notification);
        $this->em->flush();

        //Populate notification
        $this->populateNotification($notification, $notificationType);

        return $notification;
    }

    private function populateNotification(Notification $notification, NotificationType $type){
        $result = array();

        foreach($this->subscriptionService->getMembers($type) as $user){
            $link = new UserNotification($user, $notification);
            $this->em->persist($link);
            $result[] = $link;
        }
        $this->em->flush();
        return $result;
    }


}
