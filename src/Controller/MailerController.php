<?php

namespace Kalitics\NotificationBundle\Controller;

use App\Controller\BaseController;
use App\Entity\Contact\Contact;
use App\Entity\Project\Project;
use App\Enum\Mailer\MailableEntityEnum;
use Kalitics\NotificationBundle\Entity\Mail;
use Kalitics\NotificationBundle\Entity\MailableEntity;
use Kalitics\NotificationBundle\Entity\MailAttachment;
use Kalitics\NotificationBundle\Entity\MailerTemplate;
use Kalitics\NotificationBundle\Entity\MailerTemplateAttachment;
use Kalitics\NotificationBundle\Form\MailType;
use Kalitics\NotificationBundle\Twig\NotificationRuntime;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Kalitics\NotificationBundle\Service\MailerService;

class MailerController extends BaseController{

    /**
     * @return array
     */
    public static function getSubscribedServices() : array {
        return array_merge(
            parent::getSubscribedServices(),
            [
                MailerService::class => MailerService::class,
            ],
        );
    }

    /**
     * @return \Kalitics\NotificationBundle\Service\MailerService
     */
    private function getMailerService(): MailerService
    {
        /** @var MailerService $mailerService */
        $service = $this->get(MailerService::class);

        return $service;
    }

    public function historyAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        return $this->render('@kaliticsNotification/mailer/history.html.twig', []);
    }

    public function paginateAction(
        NotificationRuntime $runtime,
        Request $request
    ) {
        $output = $this->paginateRequest($request, Mail::class);

        /** @var Mail $mail */
        foreach ($output['data'] as $mail) {

            $output['result']['data'][] = [
                'id' => $mail->getId(),
                'createdAt' => $mail->getCreatedAt()->format('d/m/Y H:i'),
                'createdBy' => $mail->getCreatedBy() ? $mail->getCreatedBy()->__toString() : '',
                'mailerTemplate' => $mail->getMailerTemplate() ? $mail->getMailerTemplate()->__toString() : '',
                'subject' => $mail->getSubject(),
                'to' => implode('; ', $mail->getTo()),
                'from' => implode('; ', $mail->getFrom()),
                'mailRoute' => $runtime->mailerRouteShow($mail),
            ];
        }
        return new JsonResponse($output['result']);
    }

    public function modalAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $mail = $entityManager->getRepository(Mail::class)->find($id);

        return $this->render('@kaliticsNotification/mailer/modal.html.twig', [
            'mail' => $mail,
            'default_attachment_web' => $this->getParameter('kalitics_notification.default_attachment_web'),
            'attachment_web' => $this->getParameter('kalitics_notification.mail_attachment_web'),
        ]);
    }

    public function showAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $mail = $entityManager->getRepository(Mail::class)->find($id);

        return $this->render('@kaliticsNotification/mailer/show.html.twig', [
            'mail' => $mail,
        ]);
    }

    public function injectAction(Request $request)
    {
        $mailerService = $this->getMailerService();

        $entityManager = $this->getDoctrine()->getManager();

        $mail = new Mail();

        if (!isset($sessionEntity['entity_id'])) {
            $object = new $sessionEntity['class']();
        } else {
            $object = $entityManager->getRepository($sessionEntity['class'])->find($sessionEntity['entity_id']);
        }

        if (isset($sessionEntity['payload']) && is_array($sessionEntity['payload'])) {

            foreach (['id', '_action'] as $unset) {
                if (isset($sessionEntity['payload'][$unset])) {
                    unset($sessionEntity['payload'][$unset]);
                }
            }

            foreach ($sessionEntity['payload'] as $key => $value) {
                if (method_exists($object, $key)) {
                    if ($value === '[GET_CURRENT_USER]') {
                        $value = $this->getUser();
                    }

                    $object->{$key}($value);
                }
            }
        }

        $forward = $sessionEntity['controller'].'::'.$sessionEntity['method'];
        $flowTitle = $sessionEntity['flowTitle'];

        /** @var MailerTemplate $template */
        $template = $entityManager->getRepository(MailerTemplate::class)->findOneBy(['slug' => $sessionEntity['email']]);

        $mail->setMailerTemplate($template);
        $this->flowFill($mail, $template, $object);

        $mailerService->setQueued($mail);
        $mailerService->setLinked($object);

        return $this->forward($forward);
    }

    public function flowAction(Request $request)
    {
        $mailerService = $this->getMailerService();

        $entityManager = $this->getDoctrine()->getManager();

        $mail = new Mail();
        $sessionEntity = $request->getSession()->get('kalitics_session_entity');

        if (!isset($sessionEntity['entity_id'])) {
            $object = new $sessionEntity['class']();
        } else {
            $object = $entityManager->getRepository($sessionEntity['class'])->find($sessionEntity['entity_id']);
        }

        if (isset($sessionEntity['payload']) && is_array($sessionEntity['payload'])) {

            foreach (['id', '_action'] as $unset) {
                if (isset($sessionEntity['payload'][$unset])) {
                    unset($sessionEntity['payload'][$unset]);
                }
            }

            foreach ($sessionEntity['payload'] as $key => $value) {
                if (method_exists($object, $key)) {
                    if ($value === '[GET_CURRENT_USER]') {
                        $value = $this->getUser();
                    }

                    $object->{$key}($value);
                }
            }
        }

        $forward = $sessionEntity['controller'].'::'.$sessionEntity['method'];
        $flowTitle = $sessionEntity['flowTitle'];
        $flowLink = $sessionEntity['flowLink'];

        /** @var MailerTemplate $template */
        $template = $entityManager->getRepository(MailerTemplate::class)->findOneBy(['slug' => $sessionEntity['email']]);

        if ($request->isMethod(Request::METHOD_GET)) {
            $mail->setTo($template->getDefaultTo());
            $mail->setFrom($template->getDefaultFrom());
            $mail->setReplyto($template->getDefaultReplyto());
            $mail->setCc(array_merge($template->getDefaultCc(), [$this->getUser()->getEmail()]));

            $mail->setMailerTemplate($template);

            $this->flowFill($mail, $template, $object);
        }

        $form = $this->createForm(MailType::class, $mail, [
            'action' => $this->generateUrl('kalitics_notification_mailer_flow'),
            'choices' => $entityManager->getRepository(Contact::class)->findEmails(),
            'linked' => $sessionEntity['form'],
            'object' => $object,
            'attr' => [
                'enctype' => 'multipart/form-data',
            ],
            'csrf_protection' => false,
            'validation_groups' => 'flow',
        ]);

if ($request->isMethod(Request::METHOD_GET)) {
            $form->get('linked')->setData($object);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $request->isXmlHttpRequest() && $request->get('form_change')) {

            $message = '';
            $object = $form->get('linked')->getData();
            $this->flowFill($mail, $template, $object);

            return new JsonResponse([
                'subject' => $mail->getSubject(),
                'message' => $template->getHeader().$mail->getContent().$template->getFooter(),
            ]);

        } elseif ($form->isSubmitted() && $form->isValid()) {

            $object = $form->get('linked')->getData();
            $this->flowFill($mail, $template, $object);

            $mailAttachments = [];

            foreach ($form->get('mailAttachments') as $mailAttachmentForm) {
                /** @var MailAttachment $mailAttachment */
                $mailAttachment = $mailAttachmentForm->getData();
                /** @var UploadedFile $file */
                $file = $mailAttachmentForm->get('file')->getData();

                if ($file) {
                    $filename = md5(uniqid()) . '.' . $file->guessExtension();
                    $file->move($this->getParameter('kalitics_notification.mail_attachment_path'), $filename);
                    $mailAttachment->setFilename($filename);
                    $mailAttachment->setName($file->getClientOriginalName());

                    $mailAttachments[] = [
                        'name' => $mailAttachment->getName(),
                        'filename' => $mailAttachment->getFilename(),
                        'attachment_path' => $this->getParameter('kalitics_notification.mail_attachment_path'),
                    ];
                } else {
                    $mail->removeMailAttachment($mailAttachment);
                }
            }

            $mail->setAttachments($mailAttachments);
            $mailerService->setQueued($mail);
            $mailerService->setLinked($object);

            return $this->forward($forward);

        } elseif ($form->isSubmitted() && !$form->isValid()) {

            $message = 'Echec de l\'envoi du mail<div class="py-1"><small>';
            foreach ($form->getErrors(true) as $error) {
                $message .= '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill my-1 mx-1">' .$error->getMessage() .'</span>';
            }

            $this->addFlash('error', $message.'</small></div>');

            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }

        return $this->render('@kaliticsNotification/mailer/flow.html.twig', [
            'form' => $form->createView(),
            'flowTitle' => $flowTitle,
            'flowLink' => $flowLink,
            'mail' => $mail,
            'template' => $template,
        ]);

    }

    private function flowFill($mail, $template, $object)
    {
        $mailerService = $this->getMailerService();

        $mail->setMailerTemplate($template);

        $subject = $template->getSubject();
        $content = $template->getContent();

        $mail->setSubject($mailerService->valuationInjector($subject, $object));
        $mail->setContent($mailerService->valuationInjector($content, $object, true));

    }

    public function sendAction(Request $request, $mailableClass = null, $id = null)
    {
        $mailerService = $this->getMailerService();

        $entityManager = $this->getDoctrine()->getManager();

        $mail = new Mail();

        $object = null;
        $history = null;
        $template = null;
        if ($mailableClass && $id) {
            if (isset(MailableEntityEnum::getData()[$mailableClass]) && $entry = MailableEntityEnum::getData()[$mailableClass]) {
                try {
                    $object = $entityManager->getRepository($entry['entityClass'])->find($id);
                    $history = $entityManager->getRepository(Mail::class)->findBy(['entityClass' => $entry['entityClass'], 'entityId' => $id], ['createdAt' => 'DESC']);
                } catch (\Exception $exception) {

                }
            }
        }

        $form = $this->createForm(MailType::class, $mail, [
            'action' => $this->generateUrl('kalitics_notification_mailer_send', ['mailableClass' => $mailableClass, 'id' => $id]),
            'mailableClass' => $mailableClass,
            'choices' => $entityManager->getRepository(Contact::class)->findEmails(),
            'attr' => [
                'enctype' => 'multipart/form-data',
            ],
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $attachments = [];
            $mailAttachments = [];

            foreach ($form->get('mailAttachments') as $mailAttachmentForm) {
                /** @var MailAttachment $mailAttachment */
                $mailAttachment = $mailAttachmentForm->getData();
                /** @var UploadedFile $file */
                $file = $mailAttachmentForm->get('file')->getData();
                if ($file) {
                    $filename = md5(uniqid()) . '.' . $file->guessExtension();
                    $file->move($this->getParameter('kalitics_notification.mail_attachment_path'), $filename);
                    $mailAttachment->setFilename($filename);
                    $mailAttachment->setName($file->getClientOriginalName());

                    $mailAttachments[] = [
                        'name' => $mailAttachment->getName(),
                        'path' => $mailAttachment->getFilename(),
                    ];
                } else {
                    $mail->removeMailAttachment($mailAttachment);
                }
            }

            if (isset($request->get('mail')['files']) && $files = $request->get('mail')['files']) {

                $attachments = $this->attachmentsInjector($mail->getMailerTemplate(), $object);

                foreach ($attachments as $key => $value) {
                    if (!isset($files[$key])) {
                        unset($attachments[$key]);
                    }
                }
            }

            $mail->setAttachments(array_merge($attachments, $mailAttachments));

            if ($mail->getMailerTemplate()) {
                $mail->setMessage($mail->getMailerTemplate()->getHeader().$mail->getContent().$mail->getMailerTemplate()->getFooter());
            } else {
                $mail->setMessage($mail->getContent());
            }

            $entityManager->persist($mail);
            $entityManager->flush();

            $mailerService->setTo($mail->getTo())
                ->setFrom($mail->getFrom())
                ->setReplyTo($mail->getReplyto())
                ->setSubject($mail->getSubject())
                ->setBody($mail->getMessage())
            ;

            foreach ($mail->getAttachments() as $attachment) {

                $content = $path = null;
                $extension = '';

                if (isset($attachment['controller']) && isset($attachment['method'])) {
                    $params = [];
                    foreach ($attachment['params'] as $param) {
                        $params[$param] = $object->{'get'.ucfirst($param)}();
                    }

                    $content = $this->forward($attachment['controller'].'::'.$attachment['method'], $params)->getContent();
                    $extension = '.'.$attachment['extension'];
                } elseif (isset($attachment['id'])) {
                    $path = $this->getParameter('kalitics_notification.default_attachment_path').$attachment['path'];
                    $array = explode('.', $attachment['path']);
                    $extension = '.'.end($array);
                } else {
                    $path = $this->getParameter('kalitics_notification.mail_attachment_path').$attachment['path'];
                }

                if ($path && !$content) {
                    $content = file_get_contents($path);
                }

                $file = new \Swift_Attachment($content, $attachment['name'].$extension);
                $mailerService->addAttachmentFile($file);

            }

            $mailerService->send();

            $this->addFlash('success', 'Email envoyé');

            $referer = $request->headers->get('referer');
            return $this->redirect($referer);

        }
        if ($form->isSubmitted() && !$form->isValid()) {

            $message = 'Echec de l\'envoi du mail<div class="py-1"><small>';
            foreach ($form->getErrors(true) as $error) {
                $message .= '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill my-1 mx-1">' .$error->getMessage() .'</span>';
            }

            $this->addFlash('error', $message.'</small></div>');

            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }

        return $this->render('@kaliticsNotification/mailer/send.html.twig', [
            'form' => $form->createView(),
            'mailableClass' => $mailableClass,
            'id' => $id,
            'history' => $history,
            'default_attachment_web' => $this->getParameter('kalitics_notification.default_attachment_web'),
            'attachment_web' => $this->getParameter('kalitics_notification.mail_attachment_web'),
            'template' => $template,
        ]);
    }

    public function templateAction(Request $request, $templateId, $id = null)
    {
        $mailerService = $this->getMailerService();

        $entityManager = $this->getDoctrine()->getManager();

        /** @var MailerTemplate $mailerTemplate */
        $mailerTemplate = $entityManager->getRepository(MailerTemplate::class)->find($templateId);

        try {
            $object = $entityManager->getRepository(MailableEntityEnum::getData()[$mailerTemplate->getMailableEntity()->getSlug()]['entityClass'])->find($id);
        } catch (\Throwable $throwable) {
            $object = null;
        }

        $attachments = $this->attachmentsInjector($mailerTemplate, $object);

        return new JsonResponse([
            'header' => $mailerTemplate->getHeader(),
            'footer' => $mailerTemplate->getFooter(),
            'attachments' => $this->render('@kaliticsNotification/mailer/attachments.html.twig', [
                'attachments' => $attachments,
                'default_attachment_web' => $this->getParameter('kalitics_notification.default_attachment_path'),
            ])->getContent(),
            'to' => $mailerTemplate->getDefaultTo(),
            'from' => $mailerTemplate->getDefaultFrom(),
            'replyto' => $mailerTemplate->getDefaultReplyto(),
            'cc' => $mailerTemplate->getDefaultCc(),
            'subject' => $mailerService->valuationInjector($mailerTemplate->getSubject(), $object),
            'content' => $mailerService->valuationInjector($mailerTemplate->getContent(), $object),
            'readonlyto' => $mailerTemplate->getReadonlyTo(),
            'showto' => $mailerTemplate->getShowTo(),
            'readonlyfrom' => $mailerTemplate->getReadonlyFrom(),
            'showfrom' => $mailerTemplate->getShowFrom(),
            'readonlycc' => $mailerTemplate->getReadonlyCc(),
            'showcc' => $mailerTemplate->getShowCc(),
            'readonlyreplyto' => $mailerTemplate->getReadonlyReplyto(),
            'showreplyto' => $mailerTemplate->getShowReplyto(),
        ]);
    }

    private function attachmentsInjector($mailerTemplate, $object = null)
    {
        $attachments = [];
        /** @var MailerTemplateAttachment $defaultAttachment */
        foreach ($mailerTemplate->getDefaultAttachments() as $defaultAttachment) {
            $attachments[$defaultAttachment->getId()] = [
                'id' => $defaultAttachment->getId(),
                'name' => $defaultAttachment->getName(),
                'path' => $defaultAttachment->getFilename(),
            ];
        }

        /** @var MailableEntity $mailableEntity */
        if ($mailableEntity = $mailerTemplate->getMailableEntity()) {
            $attachment = $mailableEntity->getEntityClass()::getEmailableAttachment();
            if ($attachment && $object) {
                $params = [];
                foreach ($attachment['params'] as $param) {
                    $params[$param] = $object->{'get'.ucfirst($param)}();
                }
                $attachment['url'] = $this->generateUrl($attachment['route'], $params);
                $attachments['entity'] = $attachment;
            }
        }
        return $attachments;
    }

}
