<?php

namespace Kalitics\NotificationBundle\Controller;

use App\Controller\BaseController;
use Kalitics\NotificationBundle\Entity\UserNotification;
use Kalitics\NotificationBundle\Service\NotificationService;

class NotificationController extends BaseController{

    /**
     * @return array
     */
    public static function getSubscribedServices() : array {
        return array_merge(
            parent::getSubscribedServices(),
            [
                NotificationService::class => NotificationService::class,
            ],
        );
    }

    /**
     * @return \Kalitics\NotificationBundle\Service\NotificationService
     */
    private function getNotificationService(): NotificationService
    {
        /** @var NotificationService $service */
        $service = $this->get(NotificationService::class);

        return $service;
    }

    public function displayAction(){
        $notificationService = $this->getNotificationService();

        //Get notifications
        $userNotifications = $notificationService->getNotifications($this->getUser());

        //Count no read
        $countNew = 0;
        foreach ($userNotifications as $userNotif) {
            if (!$userNotif->isSeen()) {
                $countNew++;
            }
        }

        return $this->render('@KaliticsNotification/icon/_list.html.twig', [
            'notifications'     => $userNotifications,
            'countNew'          => $countNew
        ]);
    }

    public function detailAction($id){

        $notificationService = $this->getNotificationService();

        $entityManager                 = $this->getDoctrine()->getManager();
        $userNotification   = $entityManager->getRepository(UserNotification::class)->find($id);

        //Set as seen
        $userNotification   = $notificationService->markAsSeen($userNotification);

        return $this->render('@KaliticsNotification/icon/_modal_content.html.twig', [
            'userNotifications'     => $userNotification
        ]);
    }

    public function listsAction(){

        $notificationService = $this->getNotificationService();

        //Get notifications
        $userNotifications  = $notificationService->getNotifications($this->getUser());

        return $this->render('@KaliticsNotification/list/index.html.twig', [
            "userNotifications" => $userNotifications,
        ]);
    }

    public function listAction($id){

        $notificationService = $this->getNotificationService();

        //Mark as seen
        $entityManager                 = $this->getDoctrine()->getManager();
        $userNotification   = $entityManager->getRepository(UserNotification::class)->find($id);
        if (!$userNotification) {
            return $this->redirectToRoute('kalitics_notification_lists');
        }
        $notificationService->markAsSeen($userNotification);

        //Get notifications
        $userNotifications  = $notificationService->getNotifications($this->getUser());

        return $this->render('@KaliticsNotification/list/index.html.twig', [
            "userNotifications" => $userNotifications,
            "id"                => $id
        ]);
    }

}
