<?php

namespace Kalitics\NotificationBundle\Controller;

use App\Controller\BaseController;
use App\Entity\Contact\Contact;
use Kalitics\NotificationBundle\Entity\Mail;
use Kalitics\NotificationBundle\Entity\MailableEntity;
use Kalitics\NotificationBundle\Entity\MailerTemplate;
use Kalitics\NotificationBundle\Form\MailerTemplateType;
use Kalitics\NotificationBundle\Form\MailPreviewType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MailerTemplatingController extends BaseController{

    public function indexAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        return $this->render('@kaliticsNotification/templating/index.html.twig', [
            'templates' => $entityManager->getRepository(MailerTemplate::class)->findAll(),
        ]);
    }

    public function paginateAction(Request $request)
    {
        $output = $this->paginateRequest($request, MailerTemplate::class);

        /** @var MailerTemplate $mailerTemplate */
        foreach ($output['data'] as $mailerTemplate) {

            $output['result']['data'][] = [
                'actions' => $this->renderView(
                    '@KaliticsNotification/mailer-templating/listing/columns/actions.html.twig',
                    [
                        'mailerTemplate' => $mailerTemplate,
                    ]
                ),
                'id' => $mailerTemplate->getId(),
                'title' => $mailerTemplate->getTitle(),
                'description' => $mailerTemplate->getDescription(),
                'mailableEntity' => $mailerTemplate->getMailableEntity() ? $mailerTemplate->getMailableEntity()->getName() : '',
            ];
        }
        return new JsonResponse($output['result']);
    }

    public function createOrUpdateAction(Request $request, $id = null)
    {
        $entityManager = $this->getDoctrine()->getManager();


        if (!$id || ($id && !$mailerTemplate = $entityManager->getRepository(MailerTemplate::class)->find($id))) {
            $mailerTemplate = new MailerTemplate();
        }

        $form = $this->createForm(MailerTemplateType::class, $mailerTemplate, [
            'action' => $this->generateUrl('kalitics_notification_mailer_templating_form', ['id' => $id]),
            'choices' => $entityManager->getRepository(Contact::class)->findEmails(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($form->get('defaultAttachments') as $defaultAttachmentForm) {
                $defaultAttachment = $defaultAttachmentForm->getData();
                $file = $defaultAttachmentForm->get('file')->getData();
                if ($file) {
                    $filename = md5(uniqid()) . '.' . $file->guessExtension();
                    $file->move($this->getParameter('kalitics_notification.default_attachment_path'), $filename);

                    $defaultAttachment->setFilename($filename);
                }
            }

            $entityManager->persist($mailerTemplate);
            $entityManager->flush();

            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }

        return $this->render('@kaliticsNotification/templating/form.html.twig', [
            'form' => $form->createView(),
            'default_attachment_web' => $this->getParameter('kalitics_notification.default_attachment_web'),
        ]);
    }

    public function removeAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $mailerTemplate = $entityManager->getRepository(MailerTemplate::class)->get($id);

        $entityManager->remove($mailerTemplate);
        $entityManager->flush();

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    public function modalAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $mailerTemplate = $entityManager->getRepository(MailerTemplate::class)->find($id);

        return $this->render('@kaliticsNotification/templating/modal.html.twig', [
            'mailerTemplate' => $mailerTemplate,
        ]);
    }

    public function previewAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $mailerTemplate = $entityManager->getRepository(MailerTemplate::class)->find($id);

        return $this->render('@kaliticsNotification/templating/preview.html.twig', [
            'mailerTemplate' => $mailerTemplate,
        ]);
    }

    public function emailableAssociationAction(Request $request, $id)
    {

        $results = [];
        $entityManager = $this->getDoctrine()->getManager();

        /** @var MailableEntity $mailableEntity */
        if ($mailableEntity = $entityManager->getRepository(MailableEntity::class)->find($id)) {
            $results = $mailableEntity->getEntityClass()::getEmailableAssociation();
        }

        return $this->render('@kaliticsNotification/templating/emailable_association.html.twig', [
            'results' => $results,
        ]);
    }

    public function emailableAttachmentAction(Request $request, $id)
    {
        $results = [];
        $entityManager = $this->getDoctrine()->getManager();

        /** @var MailerTemplate $mailerTemplate */
        $mailerTemplate = $entityManager->getRepository(MailerTemplate::class)->find($id);
        foreach ($mailerTemplate->getDefaultAttachments() as $defaultAttachment) {
            $results[] = [
                'name' => $defaultAttachment->getName()
            ];
        }

        /** @var MailableEntity $mailableEntity */
        if ($mailableEntity = $mailerTemplate->getMailableEntity()) {
            $attachment = $mailableEntity->getEntityClass()::getEmailableAttachment();
            if ($attachment) {
                $results[] = $attachment;
            }
        }

        return $this->render('@kaliticsNotification/templating/emailable_attachment.html.twig', [
            'results' => $results,
        ]);
    }

    public function cloneAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var MailerTemplate $mailerTemplate */
        $mailerTemplate = $entityManager->getRepository(MailerTemplate::class)->find($id);

        $newMailerTemplate = clone $mailerTemplate;
        $newMailerTemplate->setDecontextualized(false);
        $newMailerTemplate->setTitle('[CLONE] '. $newMailerTemplate->getTitle());

        $entityManager->persist($newMailerTemplate);
        $entityManager->flush($newMailerTemplate);

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    public function sendAction(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        /** @var MailerTemplate $mailerTemplate */
        $mailerTemplate = $entityManager->getRepository(MailerTemplate::class)->find($id);

        $mail = new Mail();
        if ($request->isMethod(Request::METHOD_GET)) {
            $mail->setSubject($mailerTemplate->getSubject());
            $mail->setContent($mailerTemplate->getContent());
            $mail->setTo($mailerTemplate->getDefaultTo());
            $mail->setFrom($mailerTemplate->getDefaultFrom());
            $mail->setReplyto($mailerTemplate->getDefaultReplyto());
        }

        $form = $this->createForm(MailPreviewType::class, $mail, [
            'action' => $this->generateUrl('kalitics_notification_mailer_templating_send', ['id' => $id]),
            'choices' => $entityManager->getRepository(Contact::class)->findEmails(),
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

        }

        return $this->render('@kaliticsNotification/templating/send.html.twig', [
            'form' => $form->createView(),
            'mailerTemplate' => $mailerTemplate,
        ]);
    }

}
