<?php

namespace Kalitics\NotificationBundle\Controller;

use App\Controller\BaseController;
use App\Entity\User\User;
use Kalitics\NotificationBundle\Entity\NotificationType;
use Kalitics\NotificationBundle\Service\NotificationSubscriptionsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionController extends BaseController{

    /**
     * @return array
     */
    public static function getSubscribedServices() : array {
        return array_merge(
            parent::getSubscribedServices(),
            [
                NotificationSubscriptionsService::class => NotificationSubscriptionsService::class,
            ],
        );
    }

    /**
     * @return \Kalitics\NotificationBundle\Service\NotificationSubscriptionsService
     */
    private function getSubscriptionService(): NotificationSubscriptionsService
    {
        /** @var NotificationSubscriptionsService $service */
        $service = $this->get(NotificationSubscriptionsService::class);

        return $service;
    }

    public function subscribeAction($idu, $idt){
        $subscriptionService = $this->getSubscriptionService();

        $entityManager     = $this->getDoctrine()->getManager();
        $user   = $entityManager->getRepository(User::class)->find($idu);
        $type   = $entityManager->getRepository(NotificationType::class)->find($idt);

        if($user === null || $type === null){
            return new JsonResponse('user or notification type not found', Response::HTTP_NOT_FOUND);
        }

        $result = $subscriptionService->suscribeUsers([$user], $type);

        return new JsonResponse($result, Response::HTTP_OK);
    }

    public function manageAction($id){
        $subscriptionService = $this->getSubscriptionService();

        $entityManager     = $this->getDoctrine()->getManager();
        $result = [];

        //Get User
        $user   = $entityManager->getRepository(User::class)->find($id);

        //Get all possible subscription
        $types  = $entityManager->getRepository(NotificationType::class)->findAll();

        //Get user subscription
        foreach($types as $k => $type){
            $result[$k]['type']                 = $type;
            $result[$k]['hasSubscription']      = $subscriptionService->userHasSubscription($user, $type);
        }

        return $this->render('@KaliticsNotification/config/index.html.twig', [
            "data"      => $result,
            "user"      => $user
        ]);
    }

}
