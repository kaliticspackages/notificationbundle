<?php

namespace Kalitics\NotificationBundle\Entity;

use App\Entity\Utilities\EntityInterface;
use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * NotificationUser
 *
 * @ORM\Table(name="ntf_user_notifications")
 * @ORM\Entity(repositoryClass="Kalitics\NotificationBundle\Repository\UserNotificationRepository")
 */
class UserNotification implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="seenAt", type="datetime", nullable=true)
     */
    private $seenAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="notifications")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Kalitics\NotificationBundle\Entity\Notification")
     * @ORM\JoinColumn(name="notification_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $notification;

    /**
     * NotificationUser constructor.
     * @param $user
     * @param $notification
     */
    public function __construct(User $user, Notification $notification)
    {
        $this->seenAt           = null;
        $this->user             = $user;
        $this->notification     = $notification;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seenAt
     *
     * @param \DateTime $seenAt
     * @return NotificationUser
     */
    public function setSeenAt($seenAt)
    {
        $this->seenAt = $seenAt;
        return $this;
    }

    /**
     * Get seenAt
     * @return \DateTime
     */
    public function getSeenAt()
    {
        return $this->seenAt;
    }

    /**
     * @return bool
     */
    public function isSeen(){
        return $this->getSeenAt() ==  null ? false : true;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * @param mixed $notification
     */
    public function setNotification(Notification $notification)
    {
        $this->notification = $notification;
    }
}
