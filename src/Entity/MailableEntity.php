<?php

namespace Kalitics\NotificationBundle\Entity;

use App\Entity\Utilities\EnumEntity;
use ContextManager\DoctrineFilter\ContextCheck;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="ntf_mailable_entity")
 * @ORM\Entity(repositoryClass="Kalitics\NotificationBundle\Repository\MailableEntityRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\HasLifecycleCallbacks()
 * @ContextCheck(factoryFieldName="factory_id")
 */
class MailableEntity extends EnumEntity
{

    /**
     * @var string
     * @ORM\Column(name="entity_class", type="string", length=255)
     */
    private $entityClass;

    public function getEntityClass(): ?string
    {
        return $this->entityClass;
    }

    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name ?? '';
    }
}
