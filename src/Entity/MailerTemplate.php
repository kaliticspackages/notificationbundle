<?php

namespace Kalitics\NotificationBundle\Entity;

use ContextManager\Entity\ContextualizedEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ContextManager\DoctrineFilter\ContextCheck;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="ntf_mailer_template")
 * @ORM\Entity(repositoryClass="Kalitics\NotificationBundle\Repository\MailerTemplateRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\HasLifecycleCallbacks()
 * @ContextCheck(factoryFieldName="factory_id")
 */
class MailerTemplate extends ContextualizedEntity
{

    /**
     * @var integer $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var
     * @ORM\Column(type="string", nullable=true)
     */
    protected $slug;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $header;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $footer;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="Kalitics\NotificationBundle\Entity\MailableEntity")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $mailableEntity;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $defaultTo;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $defaultFrom;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $defaultReplyto;

    /**
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $defaultCc;

    /**
     * @var
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $readonlyTo = false;

    /**
     * @var
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $readonlyFrom = false;

    /**
     * @var
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $readonlyCc = false;

    /**
     * @var
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $readonlyReplyto = false;

    /**
     * @var
     * @ORM\Column(type="boolean", options={"default":true})
     */
    private $showTo = true;

    /**
     * @var
     * @ORM\Column(type="boolean", options={"default":true})
     */
    private $showFrom = true;

    /**
     * @var
     * @ORM\Column(type="boolean", options={"default":true})
     */
    private $showCc = true;

    /**
     * @var
     * @ORM\Column(type="boolean", options={"default":true})
     */
    private $showReplyto = true;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Kalitics\NotificationBundle\Entity\MailerTemplateAttachment", mappedBy="mailerTemplate", cascade={"persist"})
     */
    private $defaultAttachments;

    public function __construct()
    {
        parent::__construct();
        $this->defaultAttachments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getHeader(): ?string
    {
        return $this->header;
    }

    public function setHeader(?string $header): self
    {
        $this->header = $header;

        return $this;
    }

    public function getFooter(): ?string
    {
        return $this->footer;
    }

    public function setFooter(?string $footer): self
    {
        $this->footer = $footer;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMailableEntity(): ?MailableEntity
    {
        return $this->mailableEntity;
    }

    public function setMailableEntity(?MailableEntity $mailableEntity): self
    {
        $this->mailableEntity = $mailableEntity;

        return $this;
    }

    /**
     * @return Collection|MailerTemplateAttachment[]
     */
    public function getDefaultAttachments(): Collection
    {
        return $this->defaultAttachments;
    }

    public function addDefaultAttachment(MailerTemplateAttachment $defaultAttachment): self
    {
        if (!$this->defaultAttachments->contains($defaultAttachment)) {
            $this->defaultAttachments[] = $defaultAttachment;
            $defaultAttachment->setMailerTemplate($this);
        }

        return $this;
    }

    public function removeDefaultAttachment(MailerTemplateAttachment $defaultAttachment): self
    {
        if ($this->defaultAttachments->removeElement($defaultAttachment)) {
            // set the owning side to null (unless already changed)
            if ($defaultAttachment->getMailerTemplate() === $this) {
                $defaultAttachment->setMailerTemplate(null);
            }
        }

        return $this;
    }

    public function getDefaultTo(): ?array
    {
        return $this->defaultTo;
    }

    public function setDefaultTo(array $defaultTo): self
    {
        $this->defaultTo = $defaultTo;

        return $this;
    }

    public function getDefaultFrom(): ?array
    {
        return $this->defaultFrom;
    }

    public function setDefaultFrom(array $defaultFrom): self
    {
        $this->defaultFrom = $defaultFrom;

        return $this;
    }

    public function getDefaultReplyto(): ?array
    {
        return $this->defaultReplyto;
    }

    public function setDefaultReplyto(array $defaultReplyto): self
    {
        $this->defaultReplyto = $defaultReplyto;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDefaultCc(): ?array
    {
        return $this->defaultCc;
    }

    public function setDefaultCc(array $defaultCc): self
    {
        $this->defaultCc = $defaultCc;

        return $this;
    }

    public function getShowCc(): ?bool
    {
        return $this->showCc;
    }

    public function setShowCc(bool $showCc): self
    {
        $this->showCc = $showCc;

        return $this;
    }

    public function getShowReplyto(): ?bool
    {
        return $this->showReplyto;
    }

    public function setShowReplyto(bool $showReplyto): self
    {
        $this->showReplyto = $showReplyto;

        return $this;
    }

    public function getShowTo(): ?bool
    {
        return $this->showTo;
    }

    public function setShowTo(bool $showTo): self
    {
        $this->showTo = $showTo;

        return $this;
    }

    public function getShowFrom(): ?bool
    {
        return $this->showFrom;
    }

    public function setShowFrom(bool $showFrom): self
    {
        $this->showFrom = $showFrom;

        return $this;
    }

    public function getReadonlyTo(): ?bool
    {
        return $this->readonlyTo;
    }

    public function setReadonlyTo(bool $readonlyTo): self
    {
        $this->readonlyTo = $readonlyTo;

        return $this;
    }

    public function getReadonlyFrom(): ?bool
    {
        return $this->readonlyFrom;
    }

    public function setReadonlyFrom(bool $readonlyFrom): self
    {
        $this->readonlyFrom = $readonlyFrom;

        return $this;
    }

    public function getReadonlyCc(): ?bool
    {
        return $this->readonlyCc;
    }

    public function setReadonlyCc(bool $readonlyCc): self
    {
        $this->readonlyCc = $readonlyCc;

        return $this;
    }

    public function getReadonlyReplyto(): ?bool
    {
        return $this->readonlyReplyto;
    }

    public function setReadonlyReplyto(bool $readonlyReplyto): self
    {
        $this->readonlyReplyto = $readonlyReplyto;

        return $this;
    }
}
