<?php

namespace Kalitics\NotificationBundle\Entity;

use App\Entity\Utilities\EntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="ntf_mailer_template_attachment")
 * @ORM\Entity()
 */
class MailerTemplateAttachment implements EntityInterface
{

    /**
     * @var integer $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var
     * @ORM\Column(type="string")
     */
    protected $filename;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Kalitics\NotificationBundle\Entity\MailerTemplate", inversedBy="defaultAttachments")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $mailerTemplate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }


    public function getMailerTemplate(): ?MailerTemplate
    {
        return $this->mailerTemplate;
    }

    public function setMailerTemplate(?MailerTemplate $mailerTemplate): self
    {
        $this->mailerTemplate = $mailerTemplate;

        return $this;
    }


}
