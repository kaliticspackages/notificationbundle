<?php

namespace Kalitics\NotificationBundle\Entity;

use App\Entity\User\User;
use ContextManager\Entity\ContextualizedEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ContextManager\DoctrineFilter\ContextCheck;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="ntf_mailer_mail")
 * @ORM\Entity(repositoryClass="Kalitics\NotificationBundle\Repository\MailRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\HasLifecycleCallbacks()
 * @ContextCheck(factoryFieldName="factory_id")
 */
class Mail extends ContextualizedEntity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Kalitics\NotificationBundle\Entity\MailerTemplate")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $mailerTemplate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $entityClass;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $entityId;

    /**
     * @ORM\Column(type="json_array")
     */
    private $attachments = [];

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Kalitics\NotificationBundle\Entity\MailAttachment", mappedBy="mail", cascade={"persist"})
     */
    private $mailAttachments;

    /**
     * @ORM\Column(name="`to`", type="json_array")
     * @Assert\Count(minMessage="Destinataire manquant", min="1")
     */
    private $to = [];

    /**
     * @ORM\Column(name="`from`", type="json_array")
     * @Assert\Count(minMessage="Expéditeur manquant", min="1")
     */
    private $from = [];

    /**
     * @ORM\Column(type="json_array")
     */
    private $replyto = [];

    /**
     * @ORM\Column(name="`cc`", type="json_array")
     */
    private $cc = [];

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Sujet manquant", groups={"default"})
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Message manquant", groups={"default"})
     */
    private $content;

    public function __construct()
    {
        parent::__construct();
        $this->mailAttachments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntityClass(): ?string
    {
        return $this->entityClass;
    }

    public function setEntityClass(string $entityClass): self
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    public function getEntityId(): ?string
    {
        return $this->entityId;
    }

    public function setEntityId(string $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    public function getAttachments(): ?array
    {
        return $this->attachments;
    }

    public function setAttachments(array $attachments): self
    {
        $this->attachments = $attachments;

        return $this;
    }

    public function getTo(): ?array
    {
        return $this->to;
    }

    public function setTo(array $to): self
    {
        $this->to = $to;

        return $this;
    }

    public function getFrom(): ?array
    {
        return $this->from;
    }

    public function setFrom(array $from): self
    {
        $this->from = $from;

        return $this;
    }

    public function getReplyto(): ?array
    {
        return $this->replyto;
    }

    public function setReplyto(array $replyto): self
    {
        $this->replyto = $replyto;

        return $this;
    }

    public function getCc(): ?array
    {
        return $this->cc;
    }

    public function setCc(array $cc): self
    {
        $this->cc = $cc;

        return $this;
    }


    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getMailerTemplate(): ?MailerTemplate
    {
        return $this->mailerTemplate;
    }

    public function setMailerTemplate(?MailerTemplate $mailerTemplate): self
    {
        $this->mailerTemplate = $mailerTemplate;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|MailAttachment[]
     */
    public function getMailAttachments(): Collection
    {
        return $this->mailAttachments;
    }

    public function addMailAttachment(MailAttachment $mailAttachment): self
    {
        if (!$this->mailAttachments->contains($mailAttachment)) {
            $this->mailAttachments[] = $mailAttachment;
            $mailAttachment->setMail($this);
        }

        return $this;
    }

    public function removeMailAttachment(MailAttachment $mailAttachment): self
    {
        if ($this->mailAttachments->removeElement($mailAttachment)) {
            // set the owning side to null (unless already changed)
            if ($mailAttachment->getMail() === $this) {
                $mailAttachment->setMail(null);
            }
        }

        return $this;
    }

    public function setCreatedBy(?User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
