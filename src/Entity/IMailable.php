<?php

namespace Kalitics\NotificationBundle\Entity;

interface IMailable {

    public static function getEmailableAssociation();
    public static function getEmailableAttachment();
    public static function getEmailableRouteShow();

}