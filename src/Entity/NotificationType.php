<?php

namespace Kalitics\NotificationBundle\Entity;

use App\Entity\Utilities\EntityInterface;
use App\Entity\Security\Group;
use App\Entity\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * NotificationType
 *
 * @ORM\Table(name="ntf_notification_type")
 * @ORM\Entity(repositoryClass="Kalitics\NotificationBundle\Repository\NotificationTypeRepository")
 */
class NotificationType implements EntityInterface
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;

    /**
     * @var string
     * @ORM\Column(name="i_class", type="string", length=255)
     */
    private $class;

    /**
     * @var string
     * @ORM\Column(name="subject", type="string", length=255)
     */
    private $subject;

    /**
     * @var string
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Security\Group")
     * @ORM\JoinTable(name="ntf_notification_subscription_included_groups",
     *       joinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *      )
     */
    private $groups;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User\User")
     * @ORM\JoinTable(name="ntf_notification_subscription_included_members",
     *       joinColumns={@ORM\JoinColumn(name="subscription_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $includedUsers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User\User")
     * @ORM\JoinTable(name="ntf_notification_subscription_excluded_members",
     *       joinColumns={@ORM\JoinColumn(name="subscription_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $excludedUsers;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    protected $edited;

    public function __construct()
    {
        $this->includedUsers    = new ArrayCollection();
        $this->excludedUsers    = new ArrayCollection();
        $this->groups           = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass(string $class): void
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    //Submissions -------------------------------



    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $group
     */
    public function addGroup(Group $group)
    {
        if (!$this->groups->contains($groups)) {
            $this->groups[] = $group;
        }
        return $this;

    }

    public function removeGroup(Group $group){
        $this->groups->removeElement($group);
        return $this;
    }

    public function groupsContains(?Group $group){
        return $group == null ? false : $this->groups->contains($group);
    }

    /**
     * @return mixed
     */
    public function getIncludedUsers()
    {
        return $this->includedUsers;
    }

    public function includedUsersContains(User $user){
        return $this->includedUsers->contains($user);
    }

    public function excludedUsersContains(User $user){
        return $this->excludedUsers->contains($user);
    }

    /**
     * @return mixed
     */
    public function getExcludedUsers()
    {
        return $this->excludedUsers;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function addExcludedUser(User $user): self
    {
        if (!$this->excludedUsers->contains($user) && !$this->includedUsers->contains($user)) {
            $this->excludedUsers[] = $user;
        }
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function removeExcludedUser(User $user): self
    {
        if ($this->excludedUsers->contains($user)) {
            $this->excludedUsers->removeElement($user);
        }
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function addIncludedUser(User $user): self
    {
        if (!$this->includedUsers->contains($user) && !$this->excludedUsers->contains($user)) {
            $this->includedUsers[] = $user;
        }
        return $this;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function removeIncludedUser(User $user): self
    {
        if ($this->includedUsers->contains($user)) {
            $this->includedUsers->removeElement($user);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEdited()
    {
        return $this->edited;
    }

    /**
     * @param mixed $edited
     */
    public function setEdited($edited): void
    {
        $this->edited = $edited;
    }

    public function __toString()
    {
       return $this->getName();
    }
}
