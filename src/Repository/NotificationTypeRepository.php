<?php

namespace Kalitics\NotificationBundle\Repository;

use App\Repository\RepositoryAbstract;
use Doctrine\Persistence\ManagerRegistry;
use Kalitics\NotificationBundle\Entity\NotificationType;

/**
 * @method NotificationType|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotificationType|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotificationType[]    findAll()
 * @method NotificationType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method NotificationType      get($id, $lockMode = null, $lockVersion = null)
 * @method NotificationType      getOneBy(array $criteria, array $orderBy = null)
 */
class NotificationTypeRepository extends RepositoryAbstract
{

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, NotificationType::class);
    }

}
