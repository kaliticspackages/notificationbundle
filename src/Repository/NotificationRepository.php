<?php

namespace Kalitics\NotificationBundle\Repository;

use App\Repository\RepositoryAbstract;
use Doctrine\Persistence\ManagerRegistry;
use Kalitics\NotificationBundle\Entity\Notification;

/**
 * @method Notification|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notification|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notification[]    findAll()
 * @method Notification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Notification      get($id, $lockMode = null, $lockVersion = null)
 * @method Notification      getOneBy(array $criteria, array $orderBy = null)
 */
class NotificationRepository extends RepositoryAbstract
{

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Notification::class);
    }

}
