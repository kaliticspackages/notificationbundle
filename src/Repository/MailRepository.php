<?php

namespace Kalitics\NotificationBundle\Repository;

use App\Repository\BaseRepository;
use Doctrine\Persistence\ManagerRegistry;
use Kalitics\NotificationBundle\Entity\Mail;

/**
 * @method Mail|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mail|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mail[]    findAll()
 * @method Mail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Mail      get($id, $lockMode = null, $lockVersion = null)
 * @method Mail      getOneBy(array $criteria, array $orderBy = null)
 */
class MailRepository extends BaseRepository
{

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Mail::class);
    }

    public function configJoins()
    {
        return [
            [
                'field' => 'mailerTemplate',
                'alias' => 't',
            ],
            [
                'field' => 'createdBy',
                'alias' => 'u',
            ]
        ];
    }

    public function configColumns()
    {
        return [
            'u.email',
            'e.createdAt',
            'e.createdBy',
            't.title',
            'e.subject',
            'e.to',
            'e.from',
        ];
    }

    public function configSearch()
    {
        return [
            'e.createdAt',
            'e.subject',
            'e.message',
            'e.to',
            'e.from',
            't.title',
            't.description',
        ];
    }

}
