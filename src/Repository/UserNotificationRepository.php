<?php

namespace Kalitics\NotificationBundle\Repository;

use App\Repository\RepositoryAbstract;
use Doctrine\Persistence\ManagerRegistry;
use Kalitics\NotificationBundle\Entity\UserNotification;

/**
 * @method UserNotification|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserNotification|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserNotification[]    findAll()
 * @method UserNotification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method UserNotification      get($id, $lockMode = null, $lockVersion = null)
 * @method UserNotification      getOneBy(array $criteria, array $orderBy = null)
 */
class UserNotificationRepository extends RepositoryAbstract
{

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, UserNotification::class);
    }

}
