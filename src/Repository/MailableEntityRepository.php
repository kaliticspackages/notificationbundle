<?php

namespace Kalitics\NotificationBundle\Repository;

use App\Repository\RepositoryAbstract;
use Doctrine\Persistence\ManagerRegistry;
use Kalitics\NotificationBundle\Entity\MailableEntity;

/**
 * @method MailableEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method MailableEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method MailableEntity[]    findAll()
 * @method MailableEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method MailableEntity      get($id, $lockMode = null, $lockVersion = null)
 * @method MailableEntity      getOneBy(array $criteria, array $orderBy = null)
 */
class MailableEntityRepository extends RepositoryAbstract
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, MailableEntity::class);
    }
}
