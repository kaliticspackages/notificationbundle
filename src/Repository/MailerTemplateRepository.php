<?php

namespace Kalitics\NotificationBundle\Repository;

use App\Repository\BaseRepository;
use Doctrine\Persistence\ManagerRegistry;
use Kalitics\NotificationBundle\Entity\MailerTemplate;

/**
 * @method MailerTemplate|null find($id, $lockMode = null, $lockVersion = null)
 * @method MailerTemplate|null findOneBy(array $criteria, array $orderBy = null)
 * @method MailerTemplate[]    findAll()
 * @method MailerTemplate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method MailerTemplate      get($id, $lockMode = null, $lockVersion = null)
 * @method MailerTemplate      getOneBy(array $criteria, array $orderBy = null)
 */
class MailerTemplateRepository extends BaseRepository
{

    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, MailerTemplate::class);
    }

    public function configJoins()
    {
        return [
            [
                'field' => 'mailableEntity',
                'alias' => 't',
            ],
        ];
    }

    public function configColumns()
    {
        return [
            'e.title',
            'e.description',
            't.name',
        ];
    }

    public function configSearch()
    {
        return [
            'e.title',
            'e.description',
            't.name',
        ];
    }
}
