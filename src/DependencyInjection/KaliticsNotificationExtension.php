<?php


namespace Kalitics\NotificationBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class KaliticsNotificationExtension extends Extension
{
    /**
     * Loads a specific configuration.
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $fileLocator = new FileLocator(
            __DIR__ . '/../Resources/config',
        );
        $loader      = new YamlFileLoader(
            $container,
            $fileLocator,
        );
        $loader->load('services.yaml');

        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('kalitics_notification.default_attachment_path', $config['default_attachment_path']);
        $container->setParameter('kalitics_notification.default_attachment_web', $config['default_attachment_web']);

        $container->setParameter('kalitics_notification.mail_attachment_path', $config['mail_attachment_path']);
        $container->setParameter('kalitics_notification.mail_attachment_web', $config['mail_attachment_web']);

    }

    public function getAlias()
    {
        return 'kalitics_notification';
    }
}
