<?php

namespace Kalitics\NotificationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {

        if (method_exists(TreeBuilder::class, 'getRootNode')) {
            $treeBuilder = new TreeBuilder('kalitics_notification');
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $treeBuilder = new TreeBuilder();
            $rootNode = $treeBuilder->root('kalitics_notification');
        }

        $rootNode
            ->children()
                ->scalarNode('default_attachment_path')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('default_attachment_web')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('mail_attachment_path')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('mail_attachment_web')->isRequired()->cannotBeEmpty()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
