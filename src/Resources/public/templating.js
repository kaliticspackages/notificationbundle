$(function() {

    var tableConfig = [
        {
            'data': 'title',
            'orderable' : true,
            'render' : function(data){
                return data;
            }
        },
        {
            'data': 'description',
            'orderable' : true,
            'render' : function(data, type, row){
                return data;
            }
        },
        {
            'data': 'mailableEntity',
            'orderable' : true,
            'render' : function(data, type, row){
                return data;
            }
        },
        {
            'data': 'actions',
            'orderable' : false,
            'width': '220px',
        }
    ];

    var datatable = generateDatatable($('#template-list'), tableConfig, $('#generalSearch'), $('#template-filters'));
});
