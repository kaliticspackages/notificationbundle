/**
 * SCRIPT: EXAMPLE - EXTERNALLY POPULATE EMAIL
 * PURPOSE : GET CONTENT URL BY AJAX AND POPULATE
 */

function populateEmail(url) {

    $.get(url, function(response) {
        $.each(response, function(k, v) {
            var $stacks = $('#mail_' + k + ' option[selected]').clone();

            $('#mail_' + k + ' option').remove();

            var values = [];
            $.each(v, function(label, value) {
                values.push(value);
                $('#mail_' + k).append($('<option value="' + value + '">' + label + '</option>')).trigger('change');
            });

            $.each($stacks, function(key, value) {
                var $value = $(value).removeAttr('data-select2-id');
                if (values.indexOf($value.attr('value')) === -1) {
                    $('#mail_' + k).append($($value.get(0))).trigger('change');
                    values.push($value.attr('value'));
                }
            });
        });
    });
}

$(document).ready(function () {
    $('body').on('mailing.interface.ready', function(event, data) {

        /**
         * DATA SHOULD CONTAINS : entity id and mailableClass
         */
        console.log(data);

        if (url) {
            populateEmail(url);
        }
    }).on('mailing.interface.template.loaded', function(event, data) {

        /**
         * DATA SHOULD CONTAINS : entity id and mailableClass
         */
        console.log(data);

        if (url) {
            populateEmail(url);
        }
    });
});