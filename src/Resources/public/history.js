$(function() {

    var tableConfig = [
        {
            'width': '140px',
            'data': 'createdAt',
            'orderable' : true,
            'render' : function(data){
                return data;
            }
        },
        {
            'data': 'createdBy',
            'orderable' : true,
            'render' : function(data, type, row){
                return data;
            }
        },
        {
            'data': 'mailerTemplate',
            'orderable' : true,
            'render' : function(data, type, row){
                return data;
            }
        },
        {
            'data': 'subject',
            'orderable' : true,
            'render' : function(data, type, row){
                return data;
            }
        },
        {
            'data': 'to',
            'orderable' : true,
            'render' : function(data, type, row){
                return data;
            }
        },
        {
            'data': 'from',
            'orderable' : true,
            'render' : function(data, type, row){
                return data;
            }
        },
        {
            'data': 'id',
            'orderable' : false,
            'width': '140px',
            'render' : function(data, type, row){

                var url = $('#mail-urls').data('modal').replace('__ID__', row.id);

                var button = `
                    <a href="#" onclick="openModal('largeModal', '${url}'); return false;" class="btn btn-sm btn-icon btn-icon-md btn-label-primary">
                        <i class="fa fa-search"></i>
                    </a>
                `;

                if (row.mailRoute) {
                    button = button + `
                    <a href="${row.mailRoute}" class="btn btn-sm btn-icon btn-icon-md btn-label-primary">
                        <i class="fa fa-eye"></i>
                    </a>
                    `;
                }

                return button;
            }
        }
    ];

    var datatable = generateDatatable($('#mail-list'), tableConfig, $('#generalSearch'), $('#mail-filters'));
});
