$(function() {

    $('body').on('mailing.interface.ready', function() {
        console.log('mailing.interface.ready');

        $('#mail_linked').change(function() {
            $.post($(this).closest('form').attr('action'), $(this).closest('form').serialize()+'&form_change=true', function(response) {
                $('#mail_preview_subject').html(response.subject);
                $('#mail_preview_message').html(response.message);
            });
        });

        $('#mail_linked select').css({'width': '100%'});
        $('#mail_linked select').select2();

        $('#mail_mailerTemplate').css({'width': '100%'});
        $('#mail_mailerTemplate').select2();

        var lists = ['to', 'from', 'replyto', 'cc'];

        $.each(lists, function (k, v) {
            $('#mail_'+v).css({'width': '100%'});
            $('#mail_'+v).select2({
                multiple: true,
                tags: true,
            });
        });

        $('#mail_content').summernote({
            disableResizeEditor: true,
            followingToolbar: false,
            dialogsInBody: true,
            height: 190,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
            ],
            callbacks: {
                onChange: function(contents) {
                    $('#mail_content').val(contents).trigger('change');
                }
            }
        });

        $('#mail_content').change(function() {
            $('#mailer_preview_content').html($(this).val());
        }).trigger('change');


        var id = $('#mailer_send').data('id');
        var urlTemplate = $('#mailer_send').data('url');
        var mailableClass = $('#mailer_send').data('mailable-class');

        if (urlTemplate) {
            urlTemplate = urlTemplate.replace('__ID__', id);

            $('#mail_mailerTemplate').change(function () {
                var template_id = $(this).val();
                var lists = ['to', 'from', 'replyto', 'cc'];

                $.each(lists, function (k, v) {
                    $('#mail_' + v).closest('.bh-line-emails').removeClass('d-none').addClass('d-flex');
                    $('#mail_' + v).removeAttr('readonly');
                });

                if (template_id) {
                    var url = urlTemplate.replace('__TEMPLATE_ID__', template_id);

                    $('#mail_subject').val('');
                    $('#mail_content').summernote('code', '');
                    $('#mail_content').val(null);

                    $.each(lists, function (k, v) {
                        $('#mail_' + v).val('').trigger('change');
                    });

                    $('body').trigger('mailing.interface.template.loading', [{'id': id, 'slug': mailableClass}]);

                    $.get(url, function (response) {

                        $('#mail_preview_message').html(response.header + '<div id="mailer_preview_content">' + response.content + '</div>' + response.footer);

                        $('#mail_subject').val(response.subject);
                        $('#mail_content').summernote('code', response.content);
                        $('#mail_content').val(response.content);

                        $.each(lists, function (k, v) {

                            if (response[v]) {
                                if (response[v].length && response['readonly' + v]) {
                                    $('#mail_' + v).attr('readonly', 'readonly');
                                }
                                if (response[v].length === 0 && !response['show' + v]) {
                                    $('#mail_' + v).closest('.bh-line-emails').removeClass('d-flex').addClass('d-none');
                                }
                            }

                            $.each(response[v], function (key, value) {
                                $('#mail_' + v).append($('<option selected value="' + value + '">' + value + '</option>')).trigger('change');
                            });
                        });

                        $('#mail_default_attachments').html(response.attachments);
                        $('body').trigger('mailing.interface.template.loaded', [{'id': id, 'slug': mailableClass}]);

                    });
                }
            });
        }

        $('form').on('change', 'input[type="file"]', function(e) {
            var fileName = e.target.files[0].name;
            $(this).closest('.custom-file').find('.custom-file-label').html(fileName);
        });

        $('#mail_mailAttachments').collection({
            allow_up: false,
            allow_down: false,
            allow_duplicate: false,
            /*init_with_n_elements: 1,*/
            remove: '<a href="#" class="btn btn-default"><span class="fa fa-trash"></a>',
            add: '<a href="#" class="btn btn-default"><span class="fa fa-plus-circle"></a>',
            add_at_the_end: true,
        });

        $('.bh-mail-history').addClass('d-none');
        if ($('.bh-list-mail-history.active').length) {
            $('.bh-mail-history[data-id='+ $('.bh-list-mail-history.active').data('id') +']').removeClass('d-none');
        }

        $('#mailer_send .table.kalitics-table').dataTable({
            order: [[ 0, 'desc']],
        });

        $('.bh-list-mail-history').click(function() {
            $('.bh-list-mail-history').removeClass('active');
            $(this).addClass('active');
            $('.bh-mail-history').addClass('d-none');
            if ($('.bh-list-mail-history.active').length) {
                $('.bh-mail-history[data-id='+ $('.bh-list-mail-history.active').data('id') +']').removeClass('d-none');
            }
        });

    });

    $('.modal').on('hide.bs.modal', function() {
        $('#mail_content').summernote('destroy');
    });

});