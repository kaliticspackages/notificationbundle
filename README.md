
Notifications management

##Installation

Require the bundle using 

```
composer require kalitics/notification-bundle
```

Create the file config/packages/kalitics_notification.yaml with the following content :

```
kalitics_notification:
```

Add a new path for template in config/packages/twig.yaml

```
twig:
    ...
    paths:
        '%kernel.project_dir%/vendor/kalitics/notification-bundle/src/Ressources/views': kaliticsNotification
```

Add a new entry in config/bundles.php to register the new bundle

```
Kalitics\NotificationBundle\KaliticsNotificationBundle::class => ['all' => true],
```

Create a new file config/routes/kalitics_notification.yaml

```
_kalitics_notification:
  resource: '@KaliticsNotificationBundle/Ressources/config/routes.xml'
  prefix: /kalitics/notifications/

```

Add the following in the application header to display the notifications icon

```
{{ include('@KaliticsNotification/icon/notifications_container.html.twig') }}
```


Add the following in the application layout to have the detail modal

```
{{ include('@KaliticsNotification/_includes/_modal.html.twig') }}
```

The management of notification Subscriptions is display using the following controller :

```
{{ render(path('kalitics_notification_subscription_manage', {'id' : app.user.id})) }}
```


####important

There must be an entity User such as : ``App\Entity\User\User``

There must be an entity Group such as : ``App\Entity\Security\Group``


##Database configuration

To get properly configured, the database need some informations such as Notification type and difusion lists using the following tables :

```
ntf_notification_subscription_included_groups
ntf_notification_subscription_included_members
ntf_notification_subscription_excluded_members
```

##Usage

To trigger a notification, 

```
$this->notificationService->generateNotification(NotificationTypeEnum::CREATE_USER);
//Where NotificationTypeEnum::CREATE_USER is the notification slug configured in database
```
